package com.dartemenko.service;

import com.dartemenko.api.repository.RateRepository;
import com.dartemenko.api.service.RateService;
import com.dartemenko.domain.Rate;
import com.dartemenko.domain.dto.CourseTable;
import com.dartemenko.domain.dto.CurrencyCode;
import com.dartemenko.util.NbpClient;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @author Dmitry Artemenko.
 */
@Service
public class RateServiceImpl extends BaseServiceImpl<Rate> implements RateService {

    interface Constants {
        int DEFAULT_NBP_API_MAX_DATE_RANGE = 93;
    }

    private static final Logger logger = Logger.getLogger(RateServiceImpl.class);

    @Autowired
    private RateRepository rateRepository;

    @Override
    public List<Rate> refresh() {
        List<Rate> rates = new ArrayList<>();
        LocalDate recentDate = getMostRecentRateDate();
        LocalDate startDate = recentDate != null ? recentDate : LocalDate.now().minusDays(Constants.DEFAULT_NBP_API_MAX_DATE_RANGE);
        LocalDate endDate = LocalDate.now();
        rates.addAll(NbpClient.getNbpRates(CourseTable.A, CurrencyCode.USD, startDate, endDate).getRates());
        rates.sort(Comparator.comparing(Rate::getEffectiveDate).reversed());
        rates = missedRatesFilter(rates);
        if (!(rates.size() == 1 && rates.get(0).getEffectiveDate().equals(recentDate))) {
            rates.forEach(rate -> this.getRepository().insert(rate));
            logger.info("Inserted " + rates.size() + " entries");
        }
        return this.get();
    }


    @Override
    public List<Rate> get() {
        List<Rate> rates = super.get();
        rates.sort(Comparator.comparing(Rate::getEffectiveDate).reversed());
        return rates;
    }

    @Override
    public List<Rate> get(LocalDate startDate, LocalDate endDate) {
        return rateRepository.get(startDate, endDate);
    }

    @Override
    public LocalDate getMostRecentRateDate() {
        return rateRepository.getMostRecentRateDate();
    }

    private List<Rate> missedRatesFilter(List<Rate> rates) {
        List<Rate> result = new ArrayList<>();
        LocalDate previousRateDate = null;
        for (Rate rate : rates) {
            LocalDate currentRateDate = rate.getEffectiveDate();
            if (previousRateDate != null) {
                if (!previousRateDate.minusDays(1).isEqual(currentRateDate)) {
                    result.addAll(createMissedRates(rate, currentRateDate, previousRateDate));
                }
            }
            result.add(rate);
            previousRateDate = currentRateDate;
        }
        return result;
    }

    private List<Rate> createMissedRates(Rate lastRate, LocalDate from, LocalDate to) {
        List<Rate> result = new ArrayList<>();
        long days = from.until(to, ChronoUnit.DAYS);
        for (int i = 1; i < days; i++) {
            Rate rate = new Rate();
            rate
                    .setEffectiveDate(to.minusDays(i))
                    .setExchangeRate(lastRate.getExchangeRate());
            result.add(rate);
        }
        return result;
    }
}
