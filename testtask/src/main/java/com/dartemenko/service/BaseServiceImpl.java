package com.dartemenko.service;

import com.dartemenko.api.repository.BaseRepository;
import com.dartemenko.api.service.BaseService;
import com.dartemenko.domain.Identifiable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * @author Dmitry Artemenko.
 */
@Transactional
public abstract class BaseServiceImpl<T extends Identifiable> implements BaseService<T> {


    @Autowired
    private BaseRepository<T> repository;

    public List<T> get() {
        return repository.get();
    }

    public T get(UUID uuid) {
        return repository.get(uuid);
    }

    public T insert(T entity) {
        return repository.insert(entity);
    }

    public T update(T entity) {
        return repository.update(entity);
    }

    public Long count() {
        return repository.count();
    }

    public void delete(UUID uuid) {
        repository.delete(uuid);
    }

    protected BaseRepository<T> getRepository() {
        return repository;
    }

}
