package com.dartemenko.service;

import com.dartemenko.api.service.ExportService;
import com.dartemenko.api.service.RateService;
import com.dartemenko.api.service.ServiceFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Dmitry Artemenko.
 */
@Service
public class ServiceFacadeImpl implements ServiceFacade {

    @Autowired
    private RateService rateService;

    @Autowired
    private ExportService exportService;

    @Override
    public RateService getRateService() {
        return rateService;
    }

    @Override
    public ExportService getExportService() {
        return exportService;
    }
}
