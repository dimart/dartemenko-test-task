package com.dartemenko.service;

import com.dartemenko.api.service.ExportService;
import com.dartemenko.domain.Rate;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Arrays;
import java.util.List;

/**
 * @author Dmitry Artemenko.
 */

@Service
public class ExportServiceImpl implements ExportService {

    private Logger logger = Logger.getLogger(ExportServiceImpl.class);

    @Override
    public void writeExcelFile(List<Rate> rates) {
        try {
            Workbook workbook = generateExcelFile(rates, Arrays.asList(DEFAULT_DOCUMENT_HEADERS), DEFAULT_SHEET_NAME);
            createExcelFile(workbook);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public InputStream readExcelFile() {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(getExcelFile());
        } catch (FileNotFoundException e) {
            logger.warn("Failed to read the file!");
        }
        return inputStream;
    }

    @Override
    public File getExcelFile() {
        return new File(FILE_PATH);
    }

    private Workbook generateExcelFile(List<Rate> rates, List<String> headers, String sheetName) {
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet(sheetName);
        generateHeaders(sheet, headers);
        generateRows(sheet, rates);
        return workbook;
    }


    private void createExcelFile(Workbook workbook) throws IOException {
        File file = new File(FILE_PATH);
        if (!file.exists()) {
            file.createNewFile();
        }
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        workbook.write(fileOutputStream);
        workbook.close();
        fileOutputStream.close();
    }

    private void generateHeaders(Sheet sheet, List<String> headers) {
        Row headerRow = sheet.createRow(DEFAULT_HEADER_POSITION);
        int headerCount = 0;
        for (String header : headers) {
            headerRow.createCell(headerCount++).setCellValue(header);
        }
    }

    private static void generateRows(Sheet sheet, List<Rate> rates) {
        int rowCount = DEFAULT_ROW_POSITION;
        for (Rate rate : rates) {
            Row row = sheet.createRow(rowCount++);
            row.createCell(0).setCellValue(rate.getEffectiveDate().toString());
            row.createCell(1).setCellValue(rate.getExchangeRate());
        }
    }

}
