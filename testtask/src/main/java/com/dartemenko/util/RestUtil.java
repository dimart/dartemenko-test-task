package com.dartemenko.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author Dmitry Artemenko.
 */
public class RestUtil {

    public static LocalDate convert(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(date, formatter);
    }
}
