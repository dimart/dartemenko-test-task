package com.dartemenko.util;

import com.dartemenko.domain.dto.CourseTable;
import com.dartemenko.domain.dto.CurrencyCode;
import com.dartemenko.domain.dto.Rates;
import org.apache.log4j.Logger;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;

/**
 * @author Dmitry Artemenko.
 */
public class NbpClient {

    private final static Logger logger = Logger.getLogger(NbpClient.class);

    interface Endpoints {
        String RATES_WITH_DATE_RANGE = "http://api.nbp.pl/api/exchangerates/rates/{table}/{code}/{startDate}/{endDate}/";
    }

    public static Rates getNbpRates(CourseTable table, CurrencyCode code, LocalDate startDate, LocalDate endDate) {
        logger.info("Request to nbp api resource :" + Endpoints.RATES_WITH_DATE_RANGE + " data range: start date " + startDate + " end date " + endDate);
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(Endpoints.RATES_WITH_DATE_RANGE, Rates.class, table, code, startDate, endDate);
    }
}
