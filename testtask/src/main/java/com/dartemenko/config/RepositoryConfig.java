package com.dartemenko.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

/**
 * @author Dmitry Artemenko.
 */

@Configuration
@ComponentScan(basePackages = {"com.dartemenko"})
@EnableTransactionManagement
@PropertySource("classpath:repository.properties")
public class RepositoryConfig {

    interface PropertyConstants {
        String PACKAGES_TO_SCAN = "com.dartemenko";

        String HIBERNATE_FORMAT_SQL = "hibernate.format_sql";
        String HIBERNATE_SHOW_SQL = "hibernate.show_sql";
        String HIBERNATE_DIALECT = "hibernate.dialect";
        String HIBERNATE_HBM2DDL = "hibernate.hbm2ddl.auto";
    }

    @Autowired
    private Environment environment;

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactory.setDataSource(getDataSource());
        entityManagerFactory.setPackagesToScan(PropertyConstants.PACKAGES_TO_SCAN);
        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        entityManagerFactory.setJpaVendorAdapter(vendorAdapter);
        entityManagerFactory.setJpaProperties(getHibernateProperties());
        return entityManagerFactory;
    }

    private Properties getHibernateProperties() {
        Properties properties = new Properties();
        properties.put(PropertyConstants.HIBERNATE_FORMAT_SQL, environment.getProperty(PropertyConstants.HIBERNATE_FORMAT_SQL));
        properties.put(PropertyConstants.HIBERNATE_SHOW_SQL, environment.getProperty(PropertyConstants.HIBERNATE_SHOW_SQL));
        properties.put(PropertyConstants.HIBERNATE_DIALECT, environment.getProperty(PropertyConstants.HIBERNATE_DIALECT));
        properties.put(PropertyConstants.HIBERNATE_HBM2DDL, environment.getProperty(PropertyConstants.HIBERNATE_HBM2DDL));
        return properties;
    }

    private DataSource getDataSource() {
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        return builder
                .setType(EmbeddedDatabaseType.HSQL)
                .build();
    }

    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);
        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }
}
