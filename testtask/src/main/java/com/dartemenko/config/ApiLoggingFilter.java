package com.dartemenko.config;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author Dmitry Artemenko.
 */

@Component
@WebFilter("/api/*")
public class ApiLoggingFilter implements Filter {

    private static final Logger logger = Logger.getLogger(ApiLoggingFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        long time = System.currentTimeMillis();
        try {
            filterChain.doFilter(servletRequest, servletResponse);
        } finally {
            time = System.currentTimeMillis() - time;
            logger.info("Requested resource uri: " + ((HttpServletRequest) servletRequest).getRequestURI() + "  time: " + time + " ms");
        }
    }

    @Override
    public void destroy() {
    }

}
