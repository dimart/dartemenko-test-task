package com.dartemenko.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.*;

/**
 * Main app configuration class
 *
 * @author Dmitry Artemenko.
 */

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.dartemenko"})
public class AppConfig extends WebMvcConfigurerAdapter {

    interface Constants {
        String RESOURCE_HANDLER_RESOURCES_PATH = "/resources/";
        String RESOURCE_HANDLER_RESOURCES_FULL_PATH = "/resources/**";
        String CLIENT_APP_RESOURCE_HANDLER_RESOURCES_PATH = "/app/";
        String CLIENT_APP_RESOURCE_HANDLER_RESOURCES_FULL_PATH = "/app/**";
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**");
    }
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(Constants.RESOURCE_HANDLER_RESOURCES_FULL_PATH).addResourceLocations(Constants.RESOURCE_HANDLER_RESOURCES_PATH);
        registry.addResourceHandler(Constants.CLIENT_APP_RESOURCE_HANDLER_RESOURCES_FULL_PATH).addResourceLocations(Constants.CLIENT_APP_RESOURCE_HANDLER_RESOURCES_PATH);
    }

    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer
                .defaultContentType(MediaType.APPLICATION_JSON)
                .favorPathExtension(true);
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
