package com.dartemenko.api.service;

/**
 * Implementation of Facade pattern for getting access for all service from one place.
 *
 * @author Dmitry Artemenko.
 */
public interface ServiceFacade {

    RateService getRateService();

    ExportService getExportService();
}
