package com.dartemenko.api.service;

import com.dartemenko.domain.Identifiable;

import java.util.List;
import java.util.UUID;

/**
 * Basic service for handling requests with specific type.
 *
 * @param <T> type of domain object
 * @author Dmitry Artemenko.
 */
public interface BaseService<T extends Identifiable> {

    /**
     * Gets all entities from repository
     *
     * @return entities of specific type
     */
    List<T> get();


    /**
     * Gets entity from repository by identifier
     *
     * @param uuid of requested entity
     * @return entity with specific type
     */
    T get(UUID uuid);

    /**
     * Create new entity in repository
     *
     * @param entity with specific type
     * @return created entity
     */
    T insert(T entity);

    /**
     * Updated existing entity in repository
     *
     * @param entity with specific type
     * @return updated entity
     */
    T update(T entity);

    /**
     * Counts all entities in db
     *
     * @return the count
     */
    Long count();

    /**
     * Delete entity in repository by identifier
     *
     * @param uuid entity identifier
     */
    void delete(UUID uuid);
}
