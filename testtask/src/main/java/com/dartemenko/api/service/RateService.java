package com.dartemenko.api.service;

import com.dartemenko.domain.Rate;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Dmitry Artemenko.
 */
public interface RateService extends BaseService<Rate> {

    /**
     * Get actual rates from nbp
     *
     * @return list with new rates
     */
    List<Rate> refresh();

    /**
     * Gets all rates by date range
     *
     * @return rates of specific type
     */
    List<Rate> get(LocalDate startDate, LocalDate endDate);

    /**
     * Gets most recent rate date
     *
     * @return most recent rate date
     */
    LocalDate getMostRecentRateDate();
}
