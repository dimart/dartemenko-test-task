package com.dartemenko.api.service;

import com.dartemenko.domain.Rate;

import java.io.File;
import java.io.InputStream;
import java.util.List;

/**
 * @author Dmitry Artemenko.
 */
public interface ExportService {

    int DEFAULT_HEADER_POSITION = 0;
    int DEFAULT_ROW_POSITION = 1;
    String DEFAULT_SHEET_NAME = "Rates";
    String FILE_PATH = "result.xls";
    String[] DEFAULT_DOCUMENT_HEADERS = {"Date", "Exchange rate"};

    /**
     * Write rates to excel file
     *
     * @param rates list of rates
     */
    void writeExcelFile(List<Rate> rates);

    /**
     * Get input stream from excel file
     *
     * @return input stream
     */
    InputStream readExcelFile();

    /**
     * Get excel file with rates
     *
     * @return excel file with rates
     */
    File getExcelFile();
}
