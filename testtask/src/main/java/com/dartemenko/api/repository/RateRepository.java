package com.dartemenko.api.repository;

import com.dartemenko.domain.Rate;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Dmitry Artemenko.
 */
public interface RateRepository extends BaseRepository<Rate> {

    /**
     * Gets all rates from repository by date range
     *
     * @return rates of specific type
     */
    List<Rate> get(LocalDate startDate, LocalDate endDate);

    /**
     * Gets most recent rate date
     *
     * @return most recent rate date
     */
    LocalDate getMostRecentRateDate();
}
