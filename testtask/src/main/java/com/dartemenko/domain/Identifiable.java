package com.dartemenko.domain;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

/**
 * @author Dmitry Artemenko.
 */
@Data
@MappedSuperclass
public class Identifiable {

    interface Fields {
        String UUID = "UUID";
        String STRATEGY_CLASS = "org.hibernate.id.UUIDGenerator";
    }

    @Id
    @GeneratedValue(generator = Fields.UUID)
    @GenericGenerator(
            name = Fields.UUID,
            strategy = Fields.STRATEGY_CLASS
    )
    private UUID uuid;

}
