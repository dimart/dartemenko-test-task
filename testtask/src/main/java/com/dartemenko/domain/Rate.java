package com.dartemenko.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

/**
 * @author Dmitry Artemenko.
 */

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@Entity
@Table(name = "rate")
public class Rate extends Identifiable {

    public interface Columns {
        String EFFECTIVE_DATE = "effective_date";
        String EXCHANGE_RATE = "exchange_rate";
    }

    public interface Fields {
        String EFFECTIVE_DATE = "effectiveDate";
        String EXCHANGE_RATE = "exchangeRate";
    }

    @Column(name = Columns.EFFECTIVE_DATE, nullable = false)
    @JsonProperty(Fields.EFFECTIVE_DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate effectiveDate;

    @Column(name = Columns.EXCHANGE_RATE, nullable = false)
    @JsonProperty("mid")
    private Double exchangeRate;
}
