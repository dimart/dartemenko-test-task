package com.dartemenko.domain.dto;

import com.dartemenko.domain.Rate;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;

/**
 * @author Dmitry Artemenko.
 */

@Data
@Accessors(chain = true)
public class Rates {

    private CurrencyCode code;
    private CourseTable table;
    private String currency;
    private ArrayList<Rate> rates = new ArrayList<>();
}
