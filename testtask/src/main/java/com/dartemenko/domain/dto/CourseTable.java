package com.dartemenko.domain.dto;

/**
 * The type of table npb api table
 *
 * @author Dmitry Artemenko.
 */
public enum CourseTable {

    A("A"),
    B("B"),
    C("C");

    private String label;

    CourseTable(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
