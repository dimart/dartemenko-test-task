package com.dartemenko.domain.dto;

/**
 * The type of currency code
 *
 * @author Dmitry Artemenko.
 */

public enum CurrencyCode {

    USD("USD");

    private String label;

    CurrencyCode(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
