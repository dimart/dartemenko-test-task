package com.dartemenko.repository;

import com.dartemenko.api.repository.BaseRepository;
import com.dartemenko.domain.Identifiable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.UUID;

/**
 * @author Dmitry Artemenko.
 */
public abstract class BaseRepositoryImpl<T extends Identifiable> implements BaseRepository<T> {

    @PersistenceContext
    private EntityManager entityManager;

    public List<T> get() {
        CriteriaQuery<T> criteriaQuery = getCriteriaQuery();
        Root<T> root = criteriaQuery.from(getGenericType());
        criteriaQuery.select(root);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    public T get(UUID uuid) {
        return entityManager.find(getGenericType(), uuid);
    }

    public T insert(T entity) {
        entityManager.persist(entity);
        return entity;
    }

    public T update(T entity) {
        return entityManager.merge(entity);
    }

    public Long count() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<T> root = query.from(getGenericType());
        CriteriaQuery<Long> countQuery = query.select(builder.count(root));
        return entityManager.createQuery(countQuery).getSingleResult();
    }

    public void delete(UUID uuid) {
        T entity = get(uuid);
        if (entity != null) {
            entityManager.remove(entity);
        }
    }

    protected EntityManager getEntityManager() {
        return entityManager;
    }

    protected abstract Class<T> getGenericType();

    protected CriteriaQuery<T> getCriteriaQuery() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        return builder.createQuery(getGenericType());
    }

}
