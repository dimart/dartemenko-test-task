package com.dartemenko.repository;

import com.dartemenko.api.repository.RateRepository;
import com.dartemenko.domain.Rate;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.List;

/**
 * @author Dmitry Artemenko.
 */

@Repository
public class RateRepositoryImpl extends BaseRepositoryImpl<Rate> implements RateRepository {

    @Override
    public List<Rate> get(LocalDate startDate, LocalDate endDate) {
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Rate> criteriaQuery = builder.createQuery(getGenericType());
        Root<Rate> root = criteriaQuery.from(getGenericType());
        criteriaQuery
                .select(root)
                .orderBy(builder.desc(root.get(Rate.Fields.EFFECTIVE_DATE)))
                .where(builder.between(root.get(Rate.Fields.EFFECTIVE_DATE), startDate, endDate));
        return getEntityManager().createQuery(criteriaQuery).getResultList();
    }

    @Override
    public LocalDate getMostRecentRateDate() { CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<LocalDate> criteriaQuery = builder.createQuery(LocalDate.class);
        Root<Rate> root = criteriaQuery.from(getGenericType());
        criteriaQuery.select(builder.greatest(root.<LocalDate>get(Rate.Fields.EFFECTIVE_DATE)));
        return getEntityManager().createQuery(criteriaQuery).getSingleResult();
    }

    protected Class<Rate> getGenericType() {
        return Rate.class;
    }
}
