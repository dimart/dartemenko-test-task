package com.dartemenko.controller;

import com.dartemenko.api.service.ServiceFacade;
import com.dartemenko.domain.Rate;
import com.dartemenko.util.RestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * @author Dmitry Artemenko.
 */

@RestController
public class RatesController {

    interface Endpoints {
        String RATES = "/api/rates";
        String DATA_RANGE = "/{startDate}/{endDate}";
        String REFRESH = RATES + "/refresh";
        String RATES_BY_DATE_RANGE = RATES + DATA_RANGE;
        String RATES_EXPORT = RATES + "/export";
    }

    @Autowired
    private ServiceFacade serviceFacade;

    @GetMapping(Endpoints.REFRESH)
    public List<Rate> refreshRates() {
        return serviceFacade.getRateService().refresh();
    }

    @GetMapping(Endpoints.RATES)
    public List<Rate> getAll() {
        return serviceFacade.getRateService().get();
    }

    @GetMapping(Endpoints.RATES_BY_DATE_RANGE)
    public List<Rate> getByDateRange(@PathVariable String startDate, @PathVariable String endDate) {
        return serviceFacade.getRateService().get(RestUtil.convert(startDate), RestUtil.convert(endDate));
    }

    @PostMapping(Endpoints.RATES_EXPORT)
    public void createDocument(@RequestBody Rate[] rates) {
        serviceFacade.getExportService().writeExcelFile(Arrays.asList(rates));
    }
}
