package com.dartemenko.controller;

import com.dartemenko.api.service.ServiceFacade;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Dmitry Artemenko.
 */

@Controller
public class ExportController {

    interface Endpoints {
        String RATES_EXPORT = "/rates/export";
    }

    private static final Logger logger = Logger.getLogger(ExportController.class);

    @Autowired
    private ServiceFacade serviceFacade;

    @GetMapping(Endpoints.RATES_EXPORT)
    public void getDocument(HttpServletResponse response) {
        try {
            org.apache.commons.io.IOUtils.copy(serviceFacade.getExportService().readExcelFile(), response.getOutputStream());
            response.setHeader("Content-Disposition", "attachment; filename=\"" + serviceFacade.getExportService().getExcelFile().getName() + "\"");
            response.flushBuffer();
        } catch (IOException ignored) {
            logger.warn("Failed sending document");
        }
    }
}
