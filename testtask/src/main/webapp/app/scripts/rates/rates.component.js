/**
 * Created by dmitry on 28.05.2017.
 */

'use strict';

rates.component('rates', {
    templateUrl: 'app/views/rates/rates.template.html',
    controller: 'RatesController'
});