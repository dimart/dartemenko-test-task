/**
 * Created by dmitry on 28.05.2017.
 */

rates.service('RatesService', ['$http', function ($http) {
    var self = this;
    self.get = get;
    self.getByDateRange = getByDateRange;
    self.refresh = refresh;
    self.toExcel = toExcel;


    function get() {
        return $http.get("/api/rates");
    }

    function getByDateRange(startDate, endDate) {
        return $http.get("/api/rates/" + startDate + "/" + endDate);
    }

    function refresh() {
        return $http.get("/api/rates/refresh");
    }

    function toExcel(rates) {
        return $http.post("/api/rates/export", rates);
    }
}]);