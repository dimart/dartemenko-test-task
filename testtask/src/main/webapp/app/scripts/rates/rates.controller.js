/**
 * Created by dmitry on 28.05.2017.
 */

'use strict';

rates.controller('RatesController', ['$location', 'RatesService', '$scope', function ($location, RatesService, $scope) {
    $scope.refresh = refresh;
    $scope.download = download;
    $scope.startDate = undefined;
    $scope.endDate = undefined;

    (function () {
        getRates();
    })();

    function getRatesByDateRange(startDate, endDate) {
        var promise = RatesService.getByDateRange(startDate, endDate);
        promise.then(function (response) {
            prepareToRender(response.data);
            $scope.rates = response.data;
            updateChart();
        });
    }

    function getRates() {
        var promise = RatesService.get();
        promise.then(function (response) {
            $scope.rates = response.data;
            prepareToRender(response.data);
            updateChart();
        });
    }

    function refresh() {
        var promise = RatesService.refresh();
        promise.then(function (response) {
            prepareToRender(response.data);
            $scope.rates = response.data;
            updateChart();
        });
    }

    function prepareToRender(rates) {
        var map = {};

        function get(k) {
            return map[k];
        }

        for (var i = 0; i < rates.length; i++) {
            if (get(rates[i].mid) == undefined) {
                map[rates[i].mid] = [rates[i].effectiveDate];
            }

            if (i != 0) {
                if (rates[i].mid == rates[i - 1].mid) {
                    get(rates[i].mid).push(rates[i].effectiveDate);
                }
            }
        }

        $scope.mids = Object.keys(map);
        $scope.effectiveDates = Object.values(map);
    }

    function download() {
        RatesService.toExcel($scope.rates).then(function () {
            window.location.href = "/rates/export.xls";
        });
    }

    $scope.$watchGroup(["startDate", "endDate"], function (newValues, oldValues) {
        var startDate = newValues[0];
        var endDate = newValues[1];
        dateUpdated(startDate, endDate);
    });

    function dateUpdated(startDate, endDate) {
        if (endDate === undefined) {
            endDate = new Date().toISOString().slice(0, 10);
        }

        if (startDate === undefined) {
            startDate = new Date();
            startDate.setDate(startDate.getDate() - 93);
            startDate = new Date(startDate).toISOString().slice(0, 10);
        }

        getRatesByDateRange(startDate, endDate);
    }

    function updateChart() {
        var ctx = document.getElementById('ratesChart');
        var exchangeRates = angular.copy($scope.mids);
        var rawEffectiveDates = angular.copy($scope.effectiveDates);
        var effectiveDates = prepareEffectiveDateForChart(rawEffectiveDates);

        effectiveDates.reverse();
        exchangeRates.reverse();

        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: effectiveDates,
                datasets: [{
                    label: "Exchange rate: ",
                    backgroundColor: 'rgb(118, 238, 198)',
                    borderColor: 'rgb(118, 238, 198)',
                    data: exchangeRates,
                    fill: false,
                    lineTension: 0.1
                }]
            },
            options: {}
        });
    }

    function prepareEffectiveDateForChart(effectiveDays) {
        var result = [];
        effectiveDays.forEach(function (item) {
            if (item instanceof Array) {
                item.forEach(function (innerItem) {
                    result.push(innerItem);
                });
            } else {
                result.push(item);
            }
        });
        return result;
    }

}]);
