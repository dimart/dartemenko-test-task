/**
 * Created by dmitry on 28.05.2017.
 */

app.config(['$locationProvider', '$routeProvider', '$httpProvider',
    function config($locationProvider, $routeProvider, $httpProvider) {
        $locationProvider.hashPrefix('!');
        $routeProvider

            .when('/rates', {
                template: '<rates></rates>'
            })

            .otherwise('/rates', {
                template: '<rates></rates>'
            });

        $httpProvider.defaults.useXDomain = true;

    }
]);
